package com.devcamp.rest_api.Service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.devcamp.rest_api.Model.Province;
import com.devcamp.rest_api.Repository.ProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    ProvinceRepository vprovinceRepository;

    public ArrayList<Province> getProvinceList() {
        ArrayList<Province> listProvince = new ArrayList<>();
        vprovinceRepository.findAll().forEach(listProvince::add);
        return listProvince;
    }

    public ResponseEntity<Object> getCreateProvince(@RequestBody Province provinces) {
        Optional<Province> province = vprovinceRepository.findById(provinces.getId());
        if (province.isPresent()) {
            return ResponseEntity.unprocessableEntity().body(" province already exsit ");
        }
        Province province_create = vprovinceRepository.save(provinces);
        return new ResponseEntity<>(province_create, HttpStatus.CREATED);
    }

    public ResponseEntity<Object> GetupdateProvinceById(@PathVariable("id") int id,
            @RequestBody Province province) {
        Optional<Province> province1 = vprovinceRepository.findById(id);
        if (province1.isPresent()) {
            Province province2 = province1.get();
            province2.setName(province.getName());
            province2.setCode(province.getCode());
            Province province3 = vprovinceRepository.save(province2);
            try {
                return new ResponseEntity<>(province3, HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Provice:" + e.getCause().getCause().getMessage());
            }
        } else {
            return null;
        }
    }
}
