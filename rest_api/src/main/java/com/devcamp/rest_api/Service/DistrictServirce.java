package com.devcamp.rest_api.Service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestBody;

import com.devcamp.rest_api.Model.District;
import com.devcamp.rest_api.Model.Province;
import com.devcamp.rest_api.Repository.DistrictRepository;
import com.devcamp.rest_api.Repository.ProvinceRepository;

@Service
public class DistrictServirce {
    @Autowired
    DistrictRepository vdistrictRepository;

    @Autowired
    ProvinceRepository dprovinceRepository;

    public ArrayList<District> getDistrictList() {
        ArrayList<District> listDistrict = new ArrayList<>();
        vdistrictRepository.findAll().forEach(listDistrict::add);
        return listDistrict;
    }

    public District getCreateDistrict(@PathVariable("id") int id, @RequestBody District district) {
        Optional<Province> province = dprovinceRepository.findById(id);
        Optional<District> district1 = vdistrictRepository.findById(district.getId());
        if (province.isPresent()) {

            if (district1.isPresent()) {
                return null;
            }
            Province province1 = province.get();
            district.setProvince(province1);
            District district2 = vdistrictRepository.save(district);

            return district2;

        } else {
            return null;
        }

    }

    public District getUpdateDistrictById(@PathVariable("id") int id,
            @RequestBody District district) {
        Optional<District> district1 = vdistrictRepository.findById(id);
        if (district1.isPresent()) {
            District district2 = district1.get();
            district2.setName(district.getName());
            district2.setPrefix(district.getPrefix());
            // district2.setWard(district.getWard());
            District district3 = vdistrictRepository.save(district2);
            return district3;

        } else {
            return null;
        }
    }

}
