package com.devcamp.rest_api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.rest_api.Model.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer> {

}
